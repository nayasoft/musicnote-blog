var blogDetailsMap={};
var blogIdMap={};
var blogDescriptionMap={};
var blogTitleMap={};
var blogDateMap={};
var adminIdMap={};
var blogLink='';
var tempDataLength=0;
window.onBrowserHistoryButtonClicked = null;
$(document).ready(function(){
	$(".bottomFixed").attr('style','display:block;position:fixed');
	 if (window.history && window.history.pushState) {
	        $(window).on('popstate', function () {
	            if (!window.userInteractionInHTMLArea) {
	            	if(window.location.href.indexOf("#")==-1)
	            		{
	            			blogPage();
	            		}
	    if(window.onBrowserHistoryButtonClicked ){
	    window.onBrowserHistoryButtonClicked ();
	            }
	        }
	    });
	 }
	 blogPage();
});
function blogPage()
{
	$("#backButton").hide();
	var blogContent='';
	var url=urlForServer +'admin/getBlogDetails';
	var params='{"status":"A"}';
	var musicnoteAn=$.base64.encode(url);
	$.ajax({ 
		headers: { 
			"Mn-Callers" : musicnoteAn		
	 	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	data:params,
    	contentType: "application/json; charset=utf-8",
    	success : function(response){
    		
    		response=response.trim();
			if(response!=""){
    			$("#blogFullContent").empty();
    			for(var i=response.length;i<=response.length;i++){
    				
    				if(response.indexOf("[@!$[") != -1){
    					response = response.replace("[@!$[","");
					}
					if(response.indexOf("]@!$]") != -1){
						response = response.replace("]@!$]","");
					}
    				var split=response.split('~~');
    				
    					
    				
    				for(var j=0;j<split.length;j++){
    					if(split[j].indexOf("{@!${") != -1){
    						split[j] = split[j].replace("{@!${","");
						}
						if(split[j].indexOf("}@!$}") != -1){
							split[j] = split[j].replace("}@!$}","");
						}
						var values=split[j].split('~');
						var blogId='';
						var blogDescription='';
						var blogTitle='';
						var blogDate='';
						var adminId='';
						for(var k=0;k<values.length;k++){
						
							if(values[k].indexOf('*:*')){
							var blogValue=values[k].split('*:*');
							
							if(blogValue[0]=='blogId '){
								blogId=blogValue[1].trim();
							}else if(blogValue[0]=='blogTitle '){
								blogTitle=blogValue[1].trim();
							}else if(blogValue[0]=='blogDate '){
								blogDate=blogValue[1].trim();
							}else if(blogValue[0]=='blogDescription '){
								blogValue[1] = blogValue[1].split("$@$amp;").join("&amp;").split("$@$nbsp;").join("&nbsp;").split("$@$").join("&").split("$!$").join("+")
															.split("$^$").join("%").split("$-$").join("&lt;").split("$*$").join("&gt;").split("$@#$").join("~");
								blogDescription=blogValue[1].trim();
							}else if(blogValue[0]=='adminId '){
								adminId=blogValue[1].trim();
							}
							
							}
						}
						blogDetailsMap[blogId.trim()] = blogDescription.trim()+'~@~'+blogTitle.trim()+'~@~'+blogDate.trim();
						var blogUrl=blogTitle;
							blogUrl=blogUrl.replace(/ /g, '_');
						if(window.location.href.indexOf(blogUrl)!=-1)
						{
							blogFullDescription(blogId);
							break;
						}
						else
						{
							var removeTable=blogDescription;
							while(removeTable.indexOf('<table')!=-1)
							{
								if(removeTable.indexOf('<table')!=-1)
								{
									var tableDesc=removeTable.split('<table');
									tableDesc=tableDesc[1].split('</table>');
									tableDesc="<table"+tableDesc[0]+"</table>";
									removeTable=removeTable.replace(tableDesc, " ");
								}
							}
							var tempDescription=removeTable.replace(/<(?:.|\n)*?>/gm, '');
							//this is used for mobile size footer
							tempDataLength=tempDataLength+tempDescription.length;
							
							if(tempDescription.length>501 )
							{
								tempDescription = tempDescription.substring(0,500)+'...';
				    			if(j==split.length-1){
					    			blogContent=blogContent+'<div  id="'+blogId+'" ><div  id="blogTitle'+blogId+'"><h2><a class="readMoreLink" onClick="blogFullDescription('+blogId.trim()+');" style="color: inherit;text-decoration:none;" href="javascript:void(0)">'+blogTitle+'</a></h2></div><div  id="blogDate'+blogId+'" style="color:#8d8d8d;font-size:12px">'+blogDate+'</div><div  class="blogDescription" id="blogDescription'+blogId+'">'+tempDescription+'</div></br><font color="blue"><a class="readMoreLink" onClick="blogFullDescription('+blogId.trim()+');"  id="readMoreLink'+blogId+'"style="text-decoration: none !important;" href="javascript:void(0)" >Read more + </a></font>'+
					    					'</div><br>';
				    			}
								else{
									blogContent=blogContent+'<div  id="'+blogId+'" ><div  id="blogTitle'+blogId+'"><h2><a class="readMoreLink" onClick="blogFullDescription('+blogId.trim()+');" style="color: inherit;text-decoration:none;" href="javascript:void(0)">'+blogTitle+'</a></h2></div><div  id="blogDate'+blogId+'" style="color:#8d8d8d;font-size:12px">'+blogDate+'</div><div  class="blogDescription" id="blogDescription'+blogId+'">'+tempDescription+'</div></br><font color="blue"><a class="readMoreLink" onClick="blogFullDescription('+blogId.trim()+');"  id="readMoreLink'+blogId+'"style="text-decoration: none !important;" href="javascript:void(0)">Read more + </a></font>'+
									'<hr style="border-bottom: 1px solid #c7c7c7 !important;"></div>';
				    			}
		    				}
							else
							{
								if(blogDescription.indexOf('<iframe')!=-1 || blogDescription.indexOf('<embed')!=-1 || blogDescription.indexOf('<video')!=-1 || blogDescription.indexOf('<img')!=-1 || blogDescription.indexOf('.amr')!=-1 || blogDescription.indexOf('.mp3')!=-1|| blogDescription.indexOf('.wav')!=-1|| blogDescription.indexOf('.avi')!=-1 || blogDescription.indexOf('.mp4')!=-1|| blogDescription.indexOf('.wmv')!=-1 || blogDescription.indexOf('.flv')!=-1 || blogDescription.indexOf('.MPG')!=-1 || blogDescription.indexOf('href')!=-1 || blogDescription.indexOf('<a')!=-1 || blogDescription.indexOf('<table')!=-1)
								{
									if(j==split.length-1){
				    					blogContent=blogContent+'<div  id="'+blogId+'" ><div  id="blogTitle'+blogId+'"><h2><a class="readMoreLink" onClick="blogFullDescription('+blogId.trim()+');" style="color: inherit;text-decoration:none;" href="javascript:void(0)">'+blogTitle+'</a></h2></div><div  id="blogDate'+blogId+'" style="color:#8d8d8d;font-size:12px">'+blogDate+'</div><div  class="blogDescription" id="blogDescription'+blogId+'">'+tempDescription+'</div></br><font color="blue"><a class="readMoreLink" onClick="blogFullDescription('+blogId.trim()+');"  id="readMoreLink'+blogId+'"style="text-decoration: none !important;" href="javascript:void(0)">Read more + </a></font>'+
				    								'</div><br>';
				    				}
									else{
										blogContent=blogContent+'<div  id="'+blogId+'" ><div  id="blogTitle'+blogId+'"><h2><a class="readMoreLink" onClick="blogFullDescription('+blogId.trim()+');" style="color: inherit;text-decoration:none;" href="javascript:void(0)">'+blogTitle+'</a></h2></div><div  id="blogDate'+blogId+'" style="color:#8d8d8d;font-size:12px">'+blogDate+'</div><div  class="blogDescription" id="blogDescription'+blogId+'">'+tempDescription+'</div></br><font color="blue"><a class="readMoreLink" onClick="blogFullDescription('+blogId.trim()+');"  id="readMoreLink'+blogId+'"style="text-decoration: none !important;" href="javascript:void(0)">Read more + </a></font>'+
										'<hr style="border-bottom: 1px solid #c7c7c7 !important;"></div>';
				    				}
								}
								else
								{
									if(j==split.length-1){
				    					blogContent=blogContent+'<div  id="'+blogId+'" ><div  id="blogTitle'+blogId+'"><h2><a class="readMoreLink" onClick="blogFullDescription('+blogId.trim()+');" style="color: inherit;text-decoration:none;" href="javascript:void(0)">'+blogTitle+'</a></h2></div><div  id="blogDate'+blogId+'" style="color:#8d8d8d;font-size:12px">'+blogDate+'</div><div  class="blogDescription" id="blogDescription'+blogId+'">'+tempDescription+'</div>'+
				    								'</div><br>';
				    				}
									else{
										blogContent=blogContent+'<div  id="'+blogId+'" ><div  id="blogTitle'+blogId+'"><h2><a class="readMoreLink" onClick="blogFullDescription('+blogId.trim()+');" style="color: inherit;text-decoration:none;" href="javascript:void(0)">'+blogTitle+'</a></h2></div><div  id="blogDate'+blogId+'" style="color:#8d8d8d;font-size:12px">'+blogDate+'</div><div  class="blogDescription" id="blogDescription'+blogId+'">'+tempDescription+'</div>'+
										'<hr style="border-bottom: 1px solid #c7c7c7 !important;"></div>';
				    				}
								}
							}
						}
    				}
    				//this is used for mobile size footer
    				if(tempDataLength<1500)
    				{
    					$(".bottomFixed").attr('style','display:block;position:fixed');
    				}
    			}
    			if(window.location.href.indexOf("#")==-1)
				{
    				$(".bottomFixed").removeAttr('style','display:block;position:fixed');
    				$("#blogFullContent").append(blogContent);
				}
    		}else{
    			$("#blogFullContent").empty();
    			$("#blogFullContent").append('<div id="noBlogs" style="color:red;height:100%;overflow:auto;">No blogs found</div>');	
    		}
    	},
    	error: function(e) {
    		
    		alert("Please try again later");
    	}
    });
}
function blogFullDescription(id){
	$(".bottomFixed").attr('style','display:block;');
	var blogDetailss=blogDetailsMap[id];
	var content=blogDetailss.split('~@~');
	var date=content[2];
	var title=content[1];
	var blogUrl=title;
	blogUrl=blogUrl.replace(/ /g, '_');
	location.hash = blogUrl;
	var description=content[0];
	if(isIphone || isIpad || isIpod || isAndroid)
	{
		while(description.indexOf('<embed')!=-1)
		{
			if(description.indexOf('<embed')!=-1)
			{
				var videoDesc=description.split('<embed');
				videoDesc=videoDesc[1].split('>');
				videoDesc="<embed"+videoDesc[0]+">";
				var videoSrc=videoDesc.split("src=");
				videoSrc=videoSrc[1].split(' ');
				var formatIndex=videoSrc[0].lastIndexOf('.');
				var videoFormat=videoSrc[0].substring(formatIndex+1,videoSrc[0].length-1);
				var videoUrl='<video id="video" class="youtube" >'+
				'<source src='+videoSrc[0]+' type="video/'+videoFormat+'"></source></video>'; 
				description=description.replace(videoDesc,videoUrl);
			}
		}
	}
	$("#blogFullContent").empty();
	var blogFullDescriptions='<div  id="'+id+'" ><div  id="blogTitle'+id+'"><h2>'+title+'</h2></div><br><div  id="blogDate'+id+'" style="color:#8d8d8d;font-size:12px">'+date+'</div><br><div  id="blogDescription'+id+'">'+description+'</div></br>'+
	'</div>';
	$(".bottomFixed").removeAttr('style','display:block;position:fixed');
	$("#blogFullContent").append(blogFullDescriptions);
	//this is used for mobile size footer
	if(blogFullDescriptions.length<2000)
	{
		$(".bottomFixed").attr('style','display:block;position:fixed');
	}
	tempDataLength=0;
	//$("#backButton").show();
	
}